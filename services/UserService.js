import axios from 'axios'

const apiClient = axios.create({
  baseURL: `https://api.kunstfolio.tk/`,
  withCredentials: true
})

export default {
  getToken(data) {
    return apiClient.post('oauth/token', data)
  },
  registerUser(data) {
    return apiClient.post('users/register', data)
  },
  getUserDetails() {
    return apiClient.get('users/details')
  },
  setToken(token) {
    apiClient.defaults.headers.common.Authorization = `Bearer ${token}`
  }
}
