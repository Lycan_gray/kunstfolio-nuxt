export default function({ redirect, app: { $cookies }, store }) {
  const loggedIn = $cookies.get('user')

  if (!loggedIn) {
    return redirect('/login')
  } else {
    store.dispatch('clearError')
    store.dispatch('getUserDetails').then(() => {})
  }
}
