# kunstfolio-vue

> Kunstfolio Vue &amp; Nuxt Project

## Build Setup

- Download [Node.js](https://nodejs.org/en/)
- Clone the repository. 
   With SSH: `git clone git@gitlab.com:Lycan_gray/kunstfolio-nuxt.git`.
   With HTTPS: `git clone https://gitlab.com/Lycan_gray/kunstfolio-nuxt.git`
- Go to the directory in your terminal. `cd kunstfolio-nuxt`

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

[//]: # (Don't forget to add an API + Database readme, Peter)

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
