import UserService from '@/services/UserService.js'

const user = null
export const state = () =>
  user
    ? { status: { loggedIn: true }, user, userDetails: null, error: null }
    : { status: { loggedIn: false }, user: null, error: null }

export const actions = {
  clearError({ commit, context }) {
    commit('CLEAR_ERROR')
  },
  login({ commit, context }, { email, password }) {
    commit('LOGIN_REQUEST')
    const data = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'lHQMeUCRllJc58WS80LPQxRf9gNHhTXMepTiqfSR',
      username: email,
      password: password
    }
    UserService.getToken(data).then(
      user => {
        this.$cookies.set('user', JSON.stringify(user.data))
        UserService.setToken(user.data.access_token)
        commit('LOGIN_SUCCESS', user.data)
        commit('CLEAR_ERROR')
        this.$router.push('/')
      },
      error => {
        // TODO: Add Notification Handler VueX for login / get token
        commit('LOGIN_FAILURE', error)
      }
    )
  },
  register({ commit, dispatch }, user) {
    this.$cookies.remove('user')
    commit('REGISTER_REQUEST')
    UserService.registerUser(user).then(
      user => {
        commit('REGISTER_SUCCESS')
        this.$router.push('/login')
      },
      error => {
        // TODO: Add Notification Handler VueX for register
        commit('REGISTER_FAILURE', error)
      }
    )
  },
  logout({ commit }) {
    this.$cookies.remove('user')
    commit('LOGOUT')
  }
}

export const mutations = {
  CLEAR_ERROR(state) {
    state.error = null
  },
  LOGIN_REQUEST(state, user) {
    state.status = { loggingIn: true }
    state.user = user
  },
  LOGIN_FAILURE(state, error) {
    state.status = {}
    state.user = null
    state.error = error
  },
  LOGIN_SUCCESS(state, user) {
    state.status = { loggedIn: true }
    state.user = user
  },
  REGISTER_REQUEST(state) {
    state.status = { registering: true }
  },
  REGISTER_SUCCESS(state) {
    state.status = {}
  },
  REGISTER_FAILURE(state, error) {
    state.status = {}
  },
  LOGOUT(state) {
    state.status = { loggedIn: false }
    state.error = null
    state.user = null
  }
}
