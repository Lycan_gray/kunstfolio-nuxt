import UserService from '@/services/UserService.js'

export const state = () => ({
  userData: ''
})

export const actions = {
  async getUserDetails({ commit }) {
    await UserService.getUserDetails().then(data => {
      commit('SET_USER_DETAILS', data.data)
    })
  }
}

export const mutations = {
  SET_USER_DETAILS(state, data) {
    state.userData = data
  }
}
